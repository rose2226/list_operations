#include <stdio.h>
#include <stdlib.h>

// Define the structure for a Node
struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key, value;

    // Menu-driven interface
    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &value);
            deleteByValue(&head, value);
            break;
        case 6:
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &value);
            insertAfterKey(&head, key, value);
            break;
        case 7:
            printf("Enter value after which to insert: ");
            scanf("%d", &value);
            printf("Enter new value to insert: ");
            scanf("%d", &data);
            insertAfterValue(&head, value, data);
            break;
        case 8:
            printf("Exiting...\n");
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

// Function to create a new Node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to print the linked list
void printList(struct Node *head)
{
    if (head == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    struct Node *current = head;
    while (current != NULL)
    {
        printf("%d -> ", current->number);
        current = current->next;
    }
    printf("NULL\n");
}

// Function to append a new Node to the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

// Function to prepend a new Node to the beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a Node by its key
void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    // If head node itself holds the key to be deleted
    if (current != NULL && current->number == key)
    {
        *head = current->next;
        free(current);
        return;
    }

    // Search for the key to be deleted,

    while (current != NULL && current->number != key)
    {
        prev = current;
        current = current->next;
    }

    // If the key was not present in the linked list
    if (current == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }


}

// Function to delete a Node by its value
void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    // If head node itself holds the value to be deleted
    if (current != NULL && current->number == value)
    {
        *head = current->next;
        free(current);
        return;
    }

    // Search for the value to be deleted
    while (current != NULL && current->number != value)
    {
        prev = current;
        current = current->next;
    }

    // If the value was not present in the linked list
    if (current == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }

}

// Function to insert a new Node after a given key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }
    if (current == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}

// Function to insert a new Node after a given value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }
    if (current == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}
